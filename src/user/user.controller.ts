import { Controller, Post, Get, Param, Body } from '@nestjs/common';
import { UserService } from './user.service';

@Controller('user')
export class UserController {
  constructor(private userService: UserService){}
  
  @Get('/:id')
  getUser(@Param('id') id: string) {
    return this.userService.getUser(id);
  }

  @Post()
  addUser(@Body() body: any) {
    this.userService.addUser({...body});
  }
}





// route.use('/:id/profile', ProfileRouter);
// route.post('/', UserController.addUser);
// route.get('/:id/rooms', UserController.getAllUserRooms);
// route.get('/:id', UserController.getUser);
// route.put('/:id', UserController.updateUser); // TODO check if it possible to pass another value not only id
// route.delete('/:id', UserController.removeUser);
// route.post('/joinRoom', UserController.joinRoom); // TODO change endpoint for smth other
