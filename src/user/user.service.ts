import { Injectable } from '@nestjs/common';
import { User } from './entities/user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User) public userRepo: Repository<User>,
  ){

  }
  getUser(id: string) {
    console.log('You requested users ' + id)
    return ({a:id})
  }
 
  addUser(data:any) {
    this.userRepo.save({...data})
    console.log('You added a user')
  }
}
