export default (): any => ({
  serverPort: parseInt(process.env.SERVER_PORT) || 3000,
  swaggerRoot: process.env.SWAGGER_ROOT || 'api',
  db: {
    type: 'postgres',
    host: process.env.DB_HOST,
    port: parseInt(process.env.DB_PORT) || 5432,
    database: process.env.DB_DATABASE || 'db',
    username: process.env.DB_USER || 'user',
    password: process.env.DB_PASSWORD || 'password',
    // logging: true,
  },
});