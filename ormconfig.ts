export = {
  type: 'postgres',
  host: process.env.DB_HOST,
  port: process.env.DB_PORT || 5432,
  database: process.env.DB_DATABASE,
  password: process.env.DB_PASSWORD,
  username: process.env.DB_USER,
  synchronize: true,
  entities: ['src/**/entities/**/*.ts'],
  migrations: ['migrations/**/*.ts'],
  cli: {
    entitiesDir: 'entities',
    migrationsDir: 'migrations',
  },
};